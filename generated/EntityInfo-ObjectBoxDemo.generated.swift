// Generated using the ObjectBox Swift Generator — https://objectbox.io
// DO NOT EDIT

// swiftlint:disable all
import ObjectBox

// MARK: - Entity metadata


extension UserModel: ObjectBox.__EntityRelatable {
    internal typealias EntityType = UserModel

    internal var _id: EntityId<UserModel> {
        return EntityId<UserModel>(self.id.value)
    }
}

extension UserModel: ObjectBox.EntityInspectable {
    internal typealias EntityBindingType = UserModelBinding

    /// Generated metadata used by ObjectBox to persist the entity.
    internal static var entityInfo = ObjectBox.EntityInfo(name: "UserModel", id: 1)

    internal static var entityBinding = EntityBindingType()

    fileprivate static func buildEntity(modelBuilder: ObjectBox.ModelBuilder) throws {
        let entityBuilder = try modelBuilder.entityBuilder(for: UserModel.self, id: 1, uid: 6692306066890566912)
        try entityBuilder.addProperty(name: "id", type: Id.entityPropertyType, flags: [.id], id: 1, uid: 1184654532237050624)
        try entityBuilder.addProperty(name: "name", type: String.entityPropertyType, id: 2, uid: 4886543857822209280)
        try entityBuilder.addProperty(name: "email", type: String.entityPropertyType, flags: [.unique, .indexHash, .indexed], id: 3, uid: 1067565113965875456, indexId: 1, indexUid: 2973665990534672896)
        try entityBuilder.addProperty(name: "phoneNumber", type: String.entityPropertyType, id: 4, uid: 8846728297417897472)

        try entityBuilder.lastProperty(id: 4, uid: 8846728297417897472)
    }
}

extension UserModel {
    /// Generated entity property information.
    ///
    /// You may want to use this in queries to specify fetch conditions, for example:
    ///
    ///     box.query { UserModel.id == myId }
    internal static var id: Property<UserModel, Id, Id> { return Property<UserModel, Id, Id>(propertyId: 1, isPrimaryKey: true) }
    /// Generated entity property information.
    ///
    /// You may want to use this in queries to specify fetch conditions, for example:
    ///
    ///     box.query { UserModel.name.startsWith("X") }
    internal static var name: Property<UserModel, String?, Void> { return Property<UserModel, String?, Void>(propertyId: 2, isPrimaryKey: false) }
    /// Generated entity property information.
    ///
    /// You may want to use this in queries to specify fetch conditions, for example:
    ///
    ///     box.query { UserModel.email.startsWith("X") }
    internal static var email: Property<UserModel, String?, Void> { return Property<UserModel, String?, Void>(propertyId: 3, isPrimaryKey: false) }
    /// Generated entity property information.
    ///
    /// You may want to use this in queries to specify fetch conditions, for example:
    ///
    ///     box.query { UserModel.phoneNumber.startsWith("X") }
    internal static var phoneNumber: Property<UserModel, String?, Void> { return Property<UserModel, String?, Void>(propertyId: 4, isPrimaryKey: false) }

    fileprivate func __setId(identifier: ObjectBox.Id) {
        self.id = Id(identifier)
    }
}

extension ObjectBox.Property where E == UserModel {
    /// Generated entity property information.
    ///
    /// You may want to use this in queries to specify fetch conditions, for example:
    ///
    ///     box.query { .id == myId }

    internal static var id: Property<UserModel, Id, Id> { return Property<UserModel, Id, Id>(propertyId: 1, isPrimaryKey: true) }

    /// Generated entity property information.
    ///
    /// You may want to use this in queries to specify fetch conditions, for example:
    ///
    ///     box.query { .name.startsWith("X") }

    internal static var name: Property<UserModel, String?, Void> { return Property<UserModel, String?, Void>(propertyId: 2, isPrimaryKey: false) }

    /// Generated entity property information.
    ///
    /// You may want to use this in queries to specify fetch conditions, for example:
    ///
    ///     box.query { .email.startsWith("X") }

    internal static var email: Property<UserModel, String?, Void> { return Property<UserModel, String?, Void>(propertyId: 3, isPrimaryKey: false) }

    /// Generated entity property information.
    ///
    /// You may want to use this in queries to specify fetch conditions, for example:
    ///
    ///     box.query { .phoneNumber.startsWith("X") }

    internal static var phoneNumber: Property<UserModel, String?, Void> { return Property<UserModel, String?, Void>(propertyId: 4, isPrimaryKey: false) }

}


/// Generated service type to handle persisting and reading entity data. Exposed through `UserModel.EntityBindingType`.
internal class UserModelBinding: NSObject, ObjectBox.EntityBinding {
    internal typealias EntityType = UserModel
    internal typealias IdType = Id

    override internal required init() {}

    internal func setEntityIdUnlessStruct(of entity: EntityType, to entityId: ObjectBox.Id) {
        entity.__setId(identifier: entityId)
    }

    internal func entityId(of entity: EntityType) -> ObjectBox.Id {
        return entity.id.value
    }

    internal func collect(fromEntity entity: EntityType, id: ObjectBox.Id,
                                  propertyCollector: ObjectBox.FlatBufferBuilder, store: ObjectBox.Store) {
        let propertyOffset_name = propertyCollector.prepare(string: entity.name)
        let propertyOffset_email = propertyCollector.prepare(string: entity.email)
        let propertyOffset_phoneNumber = propertyCollector.prepare(string: entity.phoneNumber)

        propertyCollector.collect(id, at: 2 + 2 * 1)
        propertyCollector.collect(dataOffset: propertyOffset_name, at: 2 + 2 * 2)
        propertyCollector.collect(dataOffset: propertyOffset_email, at: 2 + 2 * 3)
        propertyCollector.collect(dataOffset: propertyOffset_phoneNumber, at: 2 + 2 * 4)
    }

    internal func createEntity(entityReader: ObjectBox.FlatBufferReader, store: ObjectBox.Store) -> EntityType {
        let entity = UserModel()

        entity.id = entityReader.read(at: 2 + 2 * 1)
        entity.name = entityReader.read(at: 2 + 2 * 2)
        entity.email = entityReader.read(at: 2 + 2 * 3)
        entity.phoneNumber = entityReader.read(at: 2 + 2 * 4)

        return entity
    }
}


/// Helper function that allows calling Enum(rawValue: value) with a nil value, which will return nil.
fileprivate func optConstruct<T: RawRepresentable>(_ type: T.Type, rawValue: T.RawValue?) -> T? {
    guard let rawValue = rawValue else { return nil }
    return T(rawValue: rawValue)
}

// MARK: - Store setup

fileprivate func cModel() throws -> OpaquePointer {
    let modelBuilder = try ObjectBox.ModelBuilder()
    try UserModel.buildEntity(modelBuilder: modelBuilder)
    modelBuilder.lastEntity(id: 1, uid: 6692306066890566912)
    modelBuilder.lastIndex(id: 1, uid: 2973665990534672896)
    return modelBuilder.finish()
}

extension ObjectBox.Store {
    /// A store with a fully configured model. Created by the code generator with your model's metadata in place.
    ///
    /// - Parameters:
    ///   - directoryPath: Directory path to store database files in.
    ///   - maxDbSizeInKByte: Limit of on-disk space for the database files. Default is `1024 * 1024` (1 GiB).
    ///   - fileMode: UNIX-style bit mask used for the database files; default is `0o755`.
    ///   - maxReaders: Maximum amount of concurrent readers, tailored to your use case. Default is `0` (unlimited).
    internal convenience init(directoryPath: String, maxDbSizeInKByte: UInt64 = 1024 * 1024, fileMode: UInt32 = 0o755, maxReaders: UInt32 = 0) throws {
        try self.init(
            model: try cModel(),
            directory: directoryPath,
            maxDbSizeInKByte: maxDbSizeInKByte,
            fileMode: fileMode,
            maxReaders: maxReaders)
    }
}

// swiftlint:enable all
