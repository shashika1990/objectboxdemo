//
//  Consts.swift
//  ObjectBoxDemo
//
//  Created by Sajja on 12/19/19.
//

import ObjectBox

class Consts {
    private static var objectBoxStore: Store? = nil
    
    static func getObjectBoxInstance() -> Store? {
        
        if objectBoxStore != nil {
            return objectBoxStore
        } else {
            do {
               objectBoxStore = try Store.createStore()
            } catch {
                print("❌ ERROR: \(error)")
            }
            return objectBoxStore
        }
        
    }
    
}
