//
//  AddMemberViewController.swift
//  ObjectBoxDemo
//
//  Created by Sajja on 12/19/19.
//

import UIKit
import ObjectBox

//protocol NewMemberDelegate {
//    func memberSaved(user: UserModel)
//}

class AddMemberViewController: UIViewController {

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    
//    var newMemberDelegate: NewMemberDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func saveMember(_ sender: UIButton) {
        guard let store = Consts.getObjectBoxInstance() else {
            // todo: Print an error message
            return
        }
        
        let user = UserModel()
        user.name = name.text
        user.email = email.text
        user.phoneNumber = phoneNumber.text
        
        if((user.name ?? "").isEmpty || (user.email ?? "").isEmpty || (user.phoneNumber ?? "").isEmpty) {
            return
        }
        
        do {
            let userBox = store.box(for: UserModel.self)
            try userBox.put(user)
            self.dismiss(animated: true, completion: nil)
            navigationController?.popViewController(animated: true)
//            newMemberDelegate.memberSaved(user: user)
        } catch ObjectBoxError.uniqueViolation {
            showErrorAlert(message: "A member with same email already availabe.")
        } catch {
            //todo - show alert with the error
            print(error)
        }

    }
}
