//
//  UserModel.swift
//  ObjectBoxDemo
//
//  Created by Sajja on 12/19/19.
//

import Foundation
import ObjectBox


class UserModel: Entity {
    var id: Id = 0
    
    var name: String?
    
    // objectbox: unique
    var email: String?
    
    var phoneNumber: String?
    
    init() {
        self.name = nil
        self.email = nil
        self.phoneNumber = nil
    }
}
