//
//  ViewController.swift
//  ObjectBoxDemo
//
//  Created by Sajja on 12/19/19.
//

import UIKit
import ObjectBox

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ResetMembersDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var memberList = [UserModel]()
    var store: Store? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self

        guard let store = Consts.getObjectBoxInstance() else {
            return
        }
        self.store = store

    }
    
    override func viewWillAppear(_ animated: Bool) {
        do {
            let userBox = store!.box(for: UserModel.self)
            let temp = try userBox.all()
            memberList.removeAll()
            memberList.append(contentsOf: temp)
        } catch {
            print("An error occured while fetching member models")
        }
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memberList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "userItemTableCell", for: indexPath) as! MemberItemTableViewCell
        
        let member: UserModel = memberList[indexPath.row]
        cell.name?.text = member.name
        cell.email?.text = member.email
        cell.phonenumber?.text = member.phoneNumber
 
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            do {
                let userBox = store!.box(for: UserModel.self)
                let id = memberList[indexPath.row]._id
                try userBox.remove(id)
                memberList.remove(at: (indexPath.row))
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
            } catch ObjectBoxError.notFound {
                showErrorAlert(message: "Something went wrong. Please try again")
            } catch {
                print("An error occured while fetching member models")
            }
        }
    }
    
    func dataDidReset() {
        memberList.removeAll()
        tableView.reloadData()
        showInfoAlert(message: "Successful.")
    }
    
    
    var actionYes = {
        (action: UIAlertAction) in
        guard let store = Consts.getObjectBoxInstance() else {
            return
        }
        
        let box = store.box(for: UserModel.self)
        do {
            try box.removeAll()
            UIAlertAction.Holder.delegates?.dataDidReset()
        } catch {
            // todo: handle error
        }
        
    }
    

    // show confirmation alert for delete all notes button
    @IBAction func deleteAllNotes(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Do you want to reset your member list?", message: "You will not be able to recover your data.", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes",
                                      style: .cancel,
                                      delegate: self,
                                      handler: actionYes))
        
        
        alert.addAction(UIAlertAction(title: "No",
                                      style: .default,
                                      delegate: self,
                                      handler: nil))
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

class MemberItemTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phonenumber: UILabel!
}
