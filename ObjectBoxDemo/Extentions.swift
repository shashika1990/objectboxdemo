//
//  Extentions.swift
//  ObjectBoxDemo
//
//  Created by Sajja on 12/19/19.
//

import Foundation
import UIKit
import MaterialComponents.MaterialSnackbar

extension UIViewController {
    
    func showErrorAlert(message: String) {
        let alert = MDCSnackbarMessage()
        alert.text = message
        MDCSnackbarManager.show(alert)
    }
    
    func showInfoAlert(message: String) {
        let alert = MDCSnackbarMessage()
        alert.text = message
        MDCSnackbarManager.show(alert)
    }
    
}

protocol ResetMembersDelegate {
    func dataDidReset()
}

extension UIAlertAction {
//    var memberSaved: NewMemberDelegate {
//        get { return self.memberSaved }
//        set(object) { self.memberSaved = object }
//    }
    
    convenience init(
    title: String?,
    style: UIAlertAction.Style,
    delegate: ResetMembersDelegate,
    handler: ((UIAlertAction) -> Void)?) {
        self.init(
            title: title,
            style: style,
            handler: handler)

        Holder.delegates = delegate
    }
    
    struct Holder {
        static var delegates: ResetMembersDelegate? = nil
    }
    
    var delegate: ResetMembersDelegate {
        get { return (Holder.delegates ?? nil)! }
    }

}
